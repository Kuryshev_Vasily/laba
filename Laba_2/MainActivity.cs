﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using OfficeOpenXml;
using Android.Content;
using Laba_2.Baseclasses;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Linq;
using Laba_2.Actionclasses;

namespace Laba_2
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        private List<ThreatData> threadItems = new List<ThreatData>();
        private string file_path;
        private Paginator paginator;
        private int total_page;

        private ListView listView;
        private TextView text_save_message;

        private Button prevButton;
        private Button nxtButton;
        private Button btnUpdate;
        private Button btnSave;

        public MainActivity()
        {
            this.file_path = Path.Combine(
                InitializeVariableClass.path_absolute,
                InitializeVariableClass.file_name_xlsx);

            if (!this.FileAvailability(Path.Combine(InitializeVariableClass.path_absolute, InitializeVariableClass.local_storage_json)))
            {
                this.DownloadFileExcel(
                    InitializeVariableClass.path_absolute,
                    InitializeVariableClass.file_name_xlsx);
                this.threadItems = this.GetDataExcel(this.file_path);
                this.SaveDataInJson(
                    InitializeVariableClass.path_absolute,
                    InitializeVariableClass.local_storage_json);
                File.Delete(this.file_path);
            }
            else
            {
                try
                {
                    this.threadItems = this.ReadDataFromFileJson(
                        InitializeVariableClass.path_absolute,
                        InitializeVariableClass.local_storage_json);
                }
                catch (Exception e)
                {
                    this.threadItems = new List<ThreatData>();
                }
            }

            this.paginator = new Paginator(this.threadItems.Count, threadItems);
            this.total_page = this.paginator.total_items / Paginator.ITEMS_PER_PAGE;
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            this.InitializeView();
            listView.Adapter = new ThreatAdapter(
                this,
                this.paginator.generatePage(InitializeVariableClass.curretnPage));

            this.btnUpdate = FindViewById<Button>(Resource.Id.UpdateData);
            this.btnUpdate.Click += delegate
            {
                UpdateInfoClass chenged_data = this.UpdateFileExcel();
                var data_json = JsonConvert.SerializeObject(chenged_data);

                Intent intent = new Intent(this, typeof(UpdateDataActivity));
                intent.PutExtra("data", data_json);
                this.StartActivity(intent);
            };
            //var a = Environment.GetExternalStoragePublicDirectory(Environment.DirectoryDocuments).AbsolutePath;
            this.text_save_message = FindViewById<TextView>(Resource.Id.statusFile);
            //text_save_message.Text = "Файл сохранен: " +
                //(this.FileAvailability(Path.Combine(InitializeVariableClass.path_file_storage, InitializeVariableClass.name_save_file)) ? "Да" : "Нет").ToString();

            this.btnSave = FindViewById<Button>(Resource.Id.SaveData);
            btnSave.Click += delegate
            {
                string error = this.SaveDataInJson(InitializeVariableClass.path_file_storage, InitializeVariableClass.name_save_file);
                if (error.Equals("") && this.FileAvailability(Path.Combine(InitializeVariableClass.path_file_storage, InitializeVariableClass.name_save_file)))
                {
                    text_save_message.Text = "Файл сохранен!";
                }
                else
                {
                    text_save_message.Text = error; 
                }
            };
        }

        private List<ThreatData> ReadDataFromFileJson(string path, string name)
        {
            return JsonConvert.DeserializeObject<List<ThreatData>>(File.ReadAllText(Path.Combine(path, name)));
        }

        private bool FileAvailability(string path)
        {
            if (path == null || !File.Exists(path))
            {
                return false;
            }
            return true;
        }

        private string ComputeMD5Checksum(string path)
        {
            using (FileStream fs = System.IO.File.OpenRead(path))
            using (MD5 md5 = new MD5CryptoServiceProvider())
            {
                byte[] checkSum = md5.ComputeHash(fs);
                string result = BitConverter.ToString(checkSum).Replace("-", String.Empty);
                return result;
            }
        }

        private string DownloadFileExcel(string path, string name)
        {
            try { 
                var backingFile = Path.Combine(path, name);
                WebClient webClient = new WebClient();
                webClient.DownloadFile(InitializeVariableClass.url, backingFile);
                return "";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        private UpdateInfoClass UpdateFileExcel()
        {
            string file_name_check = "test_check.xlsx";
            var backingFile_check = Path.Combine(
                InitializeVariableClass.path_absolute,
                file_name_check
            );

            UpdateInfoClass changed_data = new UpdateInfoClass() { status = "Успешно"};

            if (!this.DownloadFileExcel(InitializeVariableClass.path_absolute, file_name_check).Equals(""))
            {
                changed_data.error.Add("Ошибка при скачивании файла");
            }

            List<ThreatData> data_xlsx = this.GetDataExcel(backingFile_check);
            if (this.threadItems.GetHashCode().Equals(data_xlsx.GetHashCode())){
                File.Delete(file_name_check);
                return changed_data;
            }
            else
            {
                try
                {
                    foreach (var item in data_xlsx)
                    {
                        try
                        {
                            ThreatData finded = this.threadItems.Find(elm => elm.Equals(item));
                            int index = this.threadItems.FindIndex(elm => elm.Equals(finded));
                            bool flag_change_record = false;

                            if (!finded.Threat_description.Equals(item.Threat_description))
                            {
                                changed_data.initial_value.Add(
                                    finded.Threat_name + "\n"+
                                    "Описание:\n" +
                                    finded.Threat_description + "\n\n");
                                changed_data.changes.Add(
                                    finded.Threat_name +
                                    "Описание:\n" +
                                    item.Threat_description + "\n\n");
                                this.threadItems[index].Threat_description = item.Threat_description;
                                flag_change_record = true;
                            }

                            if (!finded.Threat_source.Equals(item.Threat_source))
                            {
                                changed_data.initial_value.Add(
                                    finded.Threat_name +
                                    "Источник угроз:\n" +
                                    finded.Threat_source + "\n\n");
                                changed_data.changes.Add(
                                    finded.Threat_name +
                                    "Источник угроз:\n" +
                                    item.Threat_source + "\n\n");
                                this.threadItems[index].Threat_source = item.Threat_source;
                                flag_change_record = true;
                            }

                            if (!finded.Object_impact_threat.Equals(item.Object_impact_threat))
                            {
                                changed_data.initial_value.Add(
                                    finded.Threat_name +
                                    "Объект воздействия угрозы:\n" +
                                    this.threadItems[index].Object_impact_threat + "\n\n");
                                changed_data.initial_value.Add(
                                    finded.Threat_name +
                                    "Объект воздействия угрозы:\n" +
                                    finded.Object_impact_threat + "\n\n");
                                this.threadItems[index].Object_impact_threat = item.Object_impact_threat;
                                flag_change_record = true;
                            }

                            if (!finded.Integrity_violation.Equals(item.Integrity_violation))
                            {
                                changed_data.initial_value.Add(
                                    finded.Threat_name +
                                    "Нарушение целостности:\n" +
                                    finded.Integrity_violation + "\n\n");
                                changed_data.changes.Add(
                                    finded.Threat_name +
                                    "Нарушение целостности:\n" +
                                    item.Integrity_violation + "\n\n");
                                this.threadItems[index].Integrity_violation = item.Integrity_violation;
                                flag_change_record = true;
                            }

                            if (!finded.Breach_of_confidentiality.Equals(item.Breach_of_confidentiality))
                            {
                                changed_data.initial_value.Add(
                                    finded.Threat_name +
                                    "Нарушение конфиденциальности:\n" +
                                    finded.Breach_of_confidentiality + "\n\n");
                                changed_data.changes.Add(
                                    finded.Threat_name +
                                    "Нарушение конфиденциальности:\n" +
                                    item.Breach_of_confidentiality + "\n\n");
                                this.threadItems[index].Breach_of_confidentiality = item.Breach_of_confidentiality;
                                flag_change_record = true;
                            }

                            if (!finded.Access_violation.Equals(item.Access_violation))
                            {
                                changed_data.initial_value.Add(
                                    finded.Threat_name + "Нарушение доступности:\n" +
                                    finded.Access_violation + "\n\n"
                                );
                                changed_data.changes.Add(
                                    finded.Threat_name +
                                    "Нарушение доступности:\n" +
                                    item.Access_violation + "\n\n");
                                this.threadItems[index].Access_violation = item.Access_violation;
                                flag_change_record = true;
                            }

                            if (flag_change_record)
                            {
                                changed_data.count_entries++;
                            }
                        }
                        catch (System.NullReferenceException)
                        {
                            this.threadItems.Add(item);
                            continue;
                        }
                        catch (System.ArgumentOutOfRangeException)
                        {
                            continue;
                        }
                    }
                    }
                catch 
                {
                    changed_data.status = "Ошибка";
                    changed_data.error.Add("Ошибка чтения и записи файла\n ");
                }
            }

            int q = 0;
            foreach (var item in this.threadItems)
            {
                item.id = q;
                q++;
            }
            this.SaveDataInJson(
                InitializeVariableClass.path_absolute,
                InitializeVariableClass.local_storage_json
            );

            File.Delete(file_name_check);
            return changed_data;
        }

        private string SaveDataInJson(string path, string name_file)
        {
            string file_path_json = Path.Combine(path, name_file);
            try
            {
                using (StreamWriter sw = new StreamWriter(file_path_json, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(JsonConvert.SerializeObject(this.threadItems));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return e.Message;
            }
            return "";
        }

        private List<ThreatData> GetDataExcel(string path)
        {
            List<ThreatData> data_excel = new List<ThreatData>();

            byte[] bin = File.ReadAllBytes(path);
            using (MemoryStream stream = new MemoryStream(bin))
            using (ExcelPackage excelPackage = new ExcelPackage(stream))
            {
                foreach (ExcelWorksheet worksheet in excelPackage.Workbook.Worksheets)
                {
                    for (int i = worksheet.Dimension.Start.Row + 2; i <= worksheet.Dimension.End.Row; i++)
                    {
                        try
                        {
                            data_excel.Add(new ThreatData()
                            {
                                id = ThreatData.checker_id,
                                Threat_identifier = (worksheet.Cells[i, 1].Value != null) ? worksheet.Cells[i, 1].Value.ToString().Replace("_x000d_", "") : "",
                                Threat_name = (worksheet.Cells[i, 2].Value != null) ? worksheet.Cells[i, 2].Value.ToString().Replace("_x000d_", "") : "",
                                Threat_description = (worksheet.Cells[i, 3].Value != null) ? worksheet.Cells[i, 3].Value.ToString().Replace("_x000d_", "") : "",
                                Threat_source = (worksheet.Cells[i, 4].Value != null) ? worksheet.Cells[i, 4].Value.ToString().Replace("_x000d_", "") : "",
                                Object_impact_threat = (worksheet.Cells[i, 5].Value != null) ? worksheet.Cells[i, 5].Value.ToString().Replace("_x000d_", "") : "",
                                Breach_of_confidentiality = (worksheet.Cells[i, 6].Value != null) ? worksheet.Cells[i, 6].Value.ToString().Replace("_x000d_", "") : "",
                                Integrity_violation = (worksheet.Cells[i, 7].Value != null) ? worksheet.Cells[i, 7].Value.ToString().Replace("_x000d_", "") : "",
                                Access_violation = (worksheet.Cells[i, 8].Value != null) ? worksheet.Cells[i, 8].Value.ToString().Replace("_x000d_", "") : "",
                            });
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
            return data_excel;
        }

        private void InitializeView()
        {
            //base initialize
            this.listView = FindViewById<ListView>(Resource.Id.myListView);
            this.prevButton = FindViewById<Button>(Resource.Id.prevBtn);
            this.nxtButton = FindViewById<Button>(Resource.Id.nextBtn);

            if (InitializeVariableClass.curretnPage == 0)
            {
                this.prevButton.Enabled = false;
            }

            //Button click
            this.prevButton.Click += this.PrevBtn_Click;
            this.nxtButton.Click += this.NxtBtn_Click;
        }

        private void PrevBtn_Click(object sender, System.EventArgs e)
        {
            InitializeVariableClass.curretnPage -= 1;
            this.listView.Adapter = new ThreatAdapter(
                this,
                this.paginator.generatePage(InitializeVariableClass.curretnPage)
            );
            this.ToggleBtn();
        }

        private void NxtBtn_Click(object sender, System.EventArgs e)
        {
            InitializeVariableClass.curretnPage += 1;
            this.listView.Adapter = new ThreatAdapter(
                this,
                this.paginator.generatePage(InitializeVariableClass.curretnPage)
            );
            this.ToggleBtn();
        }

        private void ToggleBtn()
        {
            if (InitializeVariableClass.curretnPage >= 1 && InitializeVariableClass.curretnPage <= this.paginator.last_page)
            {
                this.nxtButton.Enabled = true;
                this.prevButton.Enabled = true;
            }

            if (InitializeVariableClass.curretnPage == this.total_page)
            {
                this.nxtButton.Enabled = false;
                this.prevButton.Enabled = true;
            }

            if (InitializeVariableClass.curretnPage == 0)
            {
                this.nxtButton.Enabled = true;
                this.prevButton.Enabled = false;
            }
        }
    }
}

