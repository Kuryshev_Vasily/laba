﻿using System;

namespace Laba_2
{
    public class ThreatData
    {
        public static int checker_id = 0;

        public int id { get; set; }
        public string Threat_identifier { get; set; }
        public string Threat_name { get; set; }
        public string Threat_description { get; set; }
        public string Threat_source { get; set; }
        public string Object_impact_threat { get; set; }
        public string Breach_of_confidentiality { get; set; }
        public string Integrity_violation { get; set; }
        public string Access_violation { get; set; }
    

        public bool Equals(ThreatData other)
        {
            if (other == null)
                return false;

            if (this.Threat_name == other.Threat_name)
                return true;
            else
                return false;
        }

        public override bool Equals(Object obj)
        {
            if (obj == null)
                return false;

            ThreatData threat_obj = obj as ThreatData;
            if (threat_obj == null)
                return false;
            else
                return Equals(threat_obj);
        }

        public override int GetHashCode()
        {
            return this.Threat_name.GetHashCode();
        }

        public static bool operator ==(ThreatData obj_1, ThreatData obj_2)
        {
            if (((object)obj_1) == null || ((object)obj_2) == null)
                return Object.Equals(obj_1, obj_2);

            return obj_1.Equals(obj_2);
        }

        public static bool operator !=(ThreatData obj_1, ThreatData obj_2)
        {
            if (((object)obj_1) == null || ((object)obj_2) == null)
                return !Object.Equals(obj_1, obj_2);

            return !(obj_1.Equals(obj_2));
        }
    }
}
