package md5a4ef0ecce33ea96ca3f838f93317f8c9;


public class UpdateDataActivity
	extends android.app.Activity
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"n_onCreate:(Landroid/os/Bundle;)V:GetOnCreate_Landroid_os_Bundle_Handler\n" +
			"";
		mono.android.Runtime.register ("Laba_2.Actionclasses.UpdateDataActivity, Laba_2", UpdateDataActivity.class, __md_methods);
	}


	public UpdateDataActivity ()
	{
		super ();
		if (getClass () == UpdateDataActivity.class)
			mono.android.TypeManager.Activate ("Laba_2.Actionclasses.UpdateDataActivity, Laba_2", "", this, new java.lang.Object[] {  });
	}


	public void onCreate (android.os.Bundle p0)
	{
		n_onCreate (p0);
	}

	private native void n_onCreate (android.os.Bundle p0);

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
