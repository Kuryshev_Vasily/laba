﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Laba_2.Baseclasses;
using Newtonsoft.Json;

namespace Laba_2.Actionclasses
{
    [Activity(Label = "UpdateDataActivity")]
    public class UpdateDataActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your application here
            SetContentView(Resource.Layout.update_data);

            TextView status      = FindViewById<TextView>(Resource.Id.status);
            TextView count_entry = FindViewById<TextView>(Resource.Id.count_entry);
            TextView old_data    = FindViewById<TextView>(Resource.Id.old_data);
            TextView new_data    = FindViewById<TextView>(Resource.Id.new_data);
            Button button        = FindViewById<Button>(Resource.Id.back_on_main_page);

            UpdateInfoClass updateInfoClass = new UpdateInfoClass();
            updateInfoClass = JsonConvert.DeserializeObject<UpdateInfoClass>(Intent.GetStringExtra("data"));

            status.Text = updateInfoClass.status;
            count_entry.Text = updateInfoClass.count_entries.ToString();

            if (updateInfoClass.count_entries != 0)
            {
                foreach (var i in updateInfoClass.initial_value)
                {
                    old_data.Text += i;
                }

                foreach (var i in updateInfoClass.changes)
                {
                    new_data.Text += i;
                }
            }

            button.Click += delegate
            {
                Intent intent = new Intent(this, typeof(MainActivity));
                this.StartActivity(intent);
            };
        }
    }
}
