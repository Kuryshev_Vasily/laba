﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Laba_2.Baseclasses;
using Newtonsoft.Json;

namespace Laba_2
{
    [Activity(Label = "ListItemDetailActivity")]
    public class ListItemDetailActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.list_item_detail);

            TextView threatId         = FindViewById<TextView>(Resource.Id.threadId);
            TextView name             = FindViewById<TextView>(Resource.Id.name);
            TextView description      = FindViewById<TextView>(Resource.Id.description);
            TextView source           = FindViewById<TextView>(Resource.Id.source);
            TextView iterectionObject = FindViewById<TextView>(Resource.Id.iterectionObject);
            TextView confidentiality  = FindViewById<TextView>(Resource.Id.confidentiality);
            TextView integrity        = FindViewById<TextView>(Resource.Id.integrity);
            TextView availability     = FindViewById<TextView>(Resource.Id.availability);

            ThreatData data = JsonConvert.DeserializeObject<ThreatData>(Intent.GetStringExtra("data"));

            threatId.Text         = data.Threat_identifier;
            name.Text             = data.Threat_name;
            description.Text      = data.Threat_description;
            source.Text           = data.Threat_source;
            iterectionObject.Text = data.Object_impact_threat;
            confidentiality.Text  = (data.Breach_of_confidentiality.Equals("1"))?"Да":"Нет";
            integrity.Text        = (data.Integrity_violation.Equals("1")) ? "Да" : "Нет";
            availability.Text     = (data.Access_violation.Equals("1"))?"Да":"Нет";

            Button linearLayout = FindViewById<Button>(Resource.Id.back);
            linearLayout.Click += delegate {
                Intent intent = new Intent(this, typeof(MainActivity));
                intent.PutExtra("current", InitializeVariableClass.curretnPage);
                this.StartActivity(intent);
            };
        }
    }
}
