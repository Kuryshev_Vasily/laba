﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;

namespace Laba_2
{
    [Activity(Label = "ListItemActivity")]
    public class ListItemActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.list_item);

            LinearLayout linearLayout = FindViewById<LinearLayout>(Resource.Id.linearLayout1);
            linearLayout.Click += delegate {
                Intent intent = new Intent(this, typeof(ListItemDetailActivity));
                this.StartActivity(intent);
            };
        }
    }
}
